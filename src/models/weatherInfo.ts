export interface WeatherInfoEntity {
  temperature: number;
  humidity: number;
  weatherState: string;
  wind: string;
}
