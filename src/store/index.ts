import { compose, createStore, applyMiddleware } from 'redux';
import reduxThunk from 'redux-thunk';
import { reducers } from '../reducers';

const nonTypedWindow: any = window;

// const composeEnhancers =
//   process.env.NODE_ENV !== 'production' &&
//   nonTypedWindow.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
//     ? nonTypedWindow.__REDUX_DEVTOOLS_EXTENSION__
//     : compose;

// TODO Habría que ver cómo distinguir los entornos para no habilitar en producción la extensión Redux-DevTools
const composeEnhancers =
  nonTypedWindow.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(reduxThunk)),
);
