import {
  SUN,
  CLOUD,
  RAIN,
  SNOW,
  THUNDER,
  DRIZZLE,
} from '../constants/weathers';
// eslint-disable-next-line no-unused-vars
import { WeatherInfoEntity } from '../models';
import convert from 'convert-units';

const getTemperature = (tempKelvin: number): number => {
  const t = convert(tempKelvin).from('K').to('C').toFixed(0);
  return Number(t);
};

// eslint-disable-next-line no-unused-vars
const getWeatherState = (wheather: any): string => {
  const { id } = wheather;
  
  if (id < 300) {
    return THUNDER;
  } else if (id < 400) {
    return DRIZZLE;
  } else if (id < 500) {
    return RAIN;
  } else if (id < 700) {
    return SNOW;
  } else if (id === 800) {
    return SUN;
  } else {
    return CLOUD;
  }
};

export const parseWeatherData = (weatherData: any): WeatherInfoEntity => {
  const { humidity, temp } = weatherData.main;
  const { speed } = weatherData.wind;
  const weatherState = getWeatherState(weatherData.weather[0]);
  const temperature = getTemperature(temp);

  const data: WeatherInfoEntity = {
    humidity,
    temperature,
    weatherState,
    wind: `${speed} m/s`,
  };

  return data;
};
