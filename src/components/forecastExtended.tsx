import './style.css';

import * as PropTypes from 'prop-types';
import * as React from 'react';

import ForecastItem from './forecastItem';
import { API_KEY, URL_FORECAST } from '../constants/api_url';
import transformForecast from '../services/transformForecast';

interface Props {
  city: string;
}

interface State {
  city: string;
  forecastData: any;
}

class ForecastExtendedComponent extends React.Component<Props, State> {
  static propsTypes = {
    city: PropTypes.string.isRequired,
  };

  constructor(props) {
    super(props);
    const { city } = props;
    this.state = {
      city,
      forecastData: null,
    };
  }

  componentDidMount() {
    this.updatedCity(this.props.city);
  }

  // eslint-disable-next-line camelcase
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (nextProps.city !== this.props.city) {
      this.setState({ forecastData: null });
      this.updatedCity(nextProps.city);
    }
  }

  private updatedCity(city) {
    // fetch or axios
    const urlForecast = `${URL_FORECAST}/?q=${city}&appid=${API_KEY}`;
    fetch(urlForecast)
      .then(data => {
        return data.json();
      })
      .then(weatherData => {
        const forecastData = transformForecast(weatherData);
        this.setState({ forecastData });
      });
  }

  renderForecastItemDays(forecast) {
    return forecast.map(f => (
      <ForecastItem
        key={`${f.weekDay}${f.hour}`}
        weekDay={f.weekDay}
        hour={f.hour}
        data={f.data}
      ></ForecastItem>
    ));
  }

  renderProgress() {
    return <h3>Cargando Pronóstico extendido...</h3>;
  }

  render() {
    const { city } = this.props;
    const { forecastData } = this.state;
    return (
      <div>
        <h2 className="forecast-title">Pronóstico Extendido para {city}</h2>
        {forecastData
          ? this.renderForecastItemDays(forecastData)
          : this.renderProgress()}
      </div>
    );
  }
}

export const ForecastExtended = ForecastExtendedComponent;
