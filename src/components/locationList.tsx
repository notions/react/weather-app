import * as React from 'react';
import { WeatherLocation } from './weatherLocation';
import * as PropTypes from 'prop-types';
import './style.css';

const LocationList = ({ cities, onSelectedLocation }) => {
  const strToComponent = c =>
    c.map(city => (
      <WeatherLocation
        key={city}
        city={city}
        onWeatherLocationClick={(() => onSelectedLocation(city))}
      />
    ));
  return <div className='locationList'>{strToComponent(cities)}</div>;
};
LocationList.propTypes = {
  cities: PropTypes.array.isRequired,
  onSelectedLocation: PropTypes.func.isRequired,
};

export default LocationList;
