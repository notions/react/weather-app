import * as React from 'react';
import * as PropTypes from 'prop-types';
import WeatherData from '../weatherLocation/weatherData';

const ForecastItem = ({ weekDay, hour, data }) => (
  <div>
    <h2>
      {weekDay} - {hour} hs
      <WeatherData data={data}></WeatherData>
    </h2>
  </div>
);

ForecastItem.propTypes = {
  weekDay: PropTypes.string.isRequired,
  hour: PropTypes.number.isRequired,
  data: PropTypes.exact({
    temperature: PropTypes.number,
    humidity: PropTypes.number,
    weatherState: PropTypes.string,
    wind: PropTypes.string,
  }),
};

export default ForecastItem;
