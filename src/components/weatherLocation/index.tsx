import * as React from 'react';
import { CircularProgress } from '@material-ui/core';
import Location from './location';
import WeatherData from './weatherData';
import './style.css';
import { parseWeatherData } from '../../services/transformWeather';
// eslint-disable-next-line no-unused-vars
import { WeatherInfoEntity } from '../../models';
import * as PropTypes from 'prop-types';
import getUrlWeatherByCity from '../../services/getUrlWeatherByCity';

interface Props {
  city: string;
  onWeatherLocationClick: (city: string) => {},

}

interface State {
  city: string;
  data: WeatherInfoEntity;
}

class WeatherLocationComponent extends React.Component<Props, State> {
  // Typescript doesn't allow adding static properties after declaring the class.
  // We've to declare propTypes here
  static defaultProps = {
    city: 'Madrid,es',
    onWeatherLocationClick: null,
  };

  static propsTypes = {
    city: PropTypes.string.isRequired,
    onWeatherLocationClick: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    const { city } = props;
    this.state = {
      city,
      data: null,
    };
  }

  // React lifecycle
  componentDidMount() {
    this.handleUpdateClick();
  }

  handleUpdateClick = () => {
    const apiWeather = getUrlWeatherByCity(this.state.city);
    fetch(apiWeather).then(response => {
      return response.json().then(data => {
        const newWeather = parseWeatherData(data);
        this.setState({
          city: data.name,
          data: newWeather,
        });
      });
    });
  };

  handleLocationClick = () => {
    this.props.onWeatherLocationClick(this.state.city);
  }

  render() {
    return (
      <div
        className="weatherLocationContainer"
        onClick={this.handleLocationClick}
      >
        <Location city={this.state.city} />
        {this.state.data ? (
          <WeatherData data={this.state.data} />
        ) : (
          <CircularProgress />
        )}
      </div>
    );
  }
}

export const WeatherLocation = WeatherLocationComponent;
