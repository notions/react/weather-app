import * as React from 'react';
import * as PropTypes from 'prop-types';
import './style.css';

interface Props {
  humidity: number;
  wind: string;
}

const WeatherExtraInfo = (props: Props) => {
  return (
    <div className='weatherExtraInfoContainer'>
      <span className="extraInfoText">{`Humedad: ${props.humidity}% `}</span>
      <span className="extraInfoText">{`Vientos: ${props.wind}`}</span>
    </div>
  );
};

WeatherExtraInfo.propTypes = {
  humidity: PropTypes.number.isRequired,
  wind: PropTypes.string.isRequired,
};

export default WeatherExtraInfo;
