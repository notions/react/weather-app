import * as React from 'react';
import WeatherIcons from 'react-weathericons';
import {
  CLOUD,
  SUN,
  RAIN,
  SNOW,
  THUNDER,
  DRIZZLE,
} from '../../../constants/weathers';
import * as PropTypes from 'prop-types';
import './style.css';

interface Props {
  temperature: number;
  weatherState: string;
}

const icons = {
  [CLOUD]: 'cloud',
  [SUN]: 'day-sunny',
  [RAIN]: 'rain',
  [SNOW]: 'snow',
  [THUNDER]: 'day-thunderstorm',
  [DRIZZLE]: 'drizzleday-showers',
};

const getWeatherIcon = (weatherState: string) => {
  const icon = icons[weatherState];
  const sizeIcon = '4x';

  if (icon) {
    return <WeatherIcons className="wicon" name={icon} size={sizeIcon} />;
  } else {
    return <WeatherIcons className="wicon" name="day-sunny" size="2x" />;
  }
};

const WeatherTemperature = (props: Props) => {
  return (
    <div className="weatherTemperatureContainer">
      {getWeatherIcon(props.weatherState)}
      <span className="temperature">{`${props.temperature}`}</span>
      <span className="temperatureType">{' °C'}</span>
    </div>
  );
};

WeatherTemperature.propTypes = {
  temperature: PropTypes.number.isRequired,
  weatherState: PropTypes.string.isRequired,
};

export default WeatherTemperature;
