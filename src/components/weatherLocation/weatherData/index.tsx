import * as React from 'react';
import WeatherTemperature from './weatherTemperature';
import WeatherExtraInfo from './weatherExtraInfo';
import './style.css';
import * as PropTypes from 'prop-types';

interface Props {
  data: {
    temperature: number;
    humidity: number;
    weatherState: string;
    wind: string;
  }
}

const WeatherData = ({ data: { temperature, humidity, weatherState, wind } }: Props) => {
  return (
    <div className='weatherDataContainer'>
      <WeatherTemperature temperature={temperature} weatherState={weatherState} />
      <WeatherExtraInfo humidity={humidity} wind={wind} />
    </div>
  );
};

WeatherData.propType = {
  data: PropTypes.exact({
    temperature: PropTypes.number,
    humidity: PropTypes.number,
    weatherState: PropTypes.string,
    wind: PropTypes.string,
  }),
};


export default WeatherData;
