import * as React from 'react';
import * as PropTypes from 'prop-types';
import './style.css';

interface Props {
  city: string;
}

const Location = (props: Props) => {
  return (
    <div className="locationContainer">
      <h1>{props.city}</h1>
    </div>
  );
};

Location.propTypes = {
  city: PropTypes.string.isRequired,
};

export default Location;
