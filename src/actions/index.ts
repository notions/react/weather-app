import { API_KEY, URL_FORECAST } from '../constants/api_url';
import transformForecast from '../services/transformForecast';

export const SET_CITY = 'SET_CITY';
export const SET_FORECAST_DATA = 'SET_FORECAST_DATA';

const setCity = payload => ({
  type: SET_CITY,
  payload,
});

const setForecastData = payload => ({ type: SET_FORECAST_DATA, payload });

export const setSelectedCity = (city: string) => dispatcher => {
  const urlForecast = `${URL_FORECAST}/?q=${city}&appid=${API_KEY}`;

  // activar en el estado un indicador de busqueda de datos
  dispatcher(setCity(city));

  return fetch(urlForecast)
    .then(data => {
      return data.json();
    })
    .then(weatherData => {
      const forecastData = transformForecast(weatherData);
      console.log(forecastData);
      // modificar el estado con el resultado de la petición
      dispatcher(setForecastData({ city, forecastData }));
    });
};
