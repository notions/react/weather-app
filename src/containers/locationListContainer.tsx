import * as PropTypes from 'prop-types';
import * as React from 'react';
import { connect } from 'react-redux';

import { setSelectedCity } from '../actions/';
import LocationList from '../components/locationList';

interface Props {
  cities: string[];
  setCity(city: string): void;
}

class LocationListContainer extends React.Component<Props> {
  static propsTypes = {
    setCity: PropTypes.func.isRequired,
    cities: PropTypes.array.isRequired,
  };

  handleSelectionLocation = city => {
    this.props.setCity(city);
  };

  render() {
    return (
      <LocationList
        cities={this.props.cities}
        onSelectedLocation={this.handleSelectionLocation}
      />
    );
  }
}

const mapDispatchToPropsActions = dispatch => ({
  setCity: payload => dispatch(setSelectedCity(payload)),
});

export default connect(null, mapDispatchToPropsActions)(LocationListContainer);
