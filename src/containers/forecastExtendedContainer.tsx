import * as React from 'react';
import * as PropTypes from 'prop-types';
import { ForecastExtended } from '../components/forecastExtended';
import { connect } from 'react-redux';

interface Props {
  city: string;
}

class ForecastExtendedContainer extends React.Component<Props> {
  static propsTypes = {
    city: PropTypes.string.isRequired,
  };

  render() {
    return this.props.city ? <ForecastExtended city={this.props.city} /> : null;
  }
}
const mapStateToProps = ({ city }) => ({ city });

export default connect(mapStateToProps, null)(ForecastExtendedContainer);
