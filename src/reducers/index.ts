/* eslint-disable no-unused-vars */
import { combineReducers } from 'redux';
import { citiesReducer } from './cities';
import { cityReducer, cityState } from './city';

export interface State {
  citiesReducer: any;
  cityReducer: cityState;
}

export const reducers = combineReducers<State>({
  citiesReducer,
  cityReducer,
});
