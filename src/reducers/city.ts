/* eslint-disable indent */
import { SET_CITY } from '../actions';

export interface cityState {
  city: string;
}

const defaultCityState = (): cityState => ({
  city: '',
});

export const cityReducer = (state: cityState = defaultCityState(), action) => {
  switch (action.type) {
    case SET_CITY:
      return action.payload;
    default:
      break;
  }

  return state;
};
