/* eslint-disable indent */
import { SET_FORECAST_DATA } from '../actions';

export const citiesReducer = (state = {}, action) => {
  console.log(state);
  console.log(action);
  switch (action.type) {
    case SET_FORECAST_DATA: {
      const { city, forecastData } = action.payload;
      return {
        ...state,
        [city]: { forecastData },
      };
    }
    default:
      break;
  }
  return state;
};
