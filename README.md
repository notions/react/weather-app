# Weather Forecast App

[![pipeline status](https://gitlab.com/notions/react/weather-app/badges/develop/pipeline.svg)](https://gitlab.com/notions/react/weather-app/-/commits/develop)

Get the weather forecast on your smartphone.

Made with React!

## Getting started

In order to run this project you have to install the follogin requirements:

* [Node.js](https://nodejs.org/es/)
* [Yarn](https://yarnpkg.com/)
* [Git](https://git-scm.com/)

Once you have installed the requirements, you have to clone or download this repo:

```bash
git clone https://gitlab.com/notions/react/weather-app.git
```

Then you have to install project's dependencies:

```bash
cd weather-app
yarn
```

or

```bash
yarn install
```

> Alternatively you can use `npm` instead of `yarn`.

Once project's dependencies have been installed, you can run this command to start the application:

```bash
yarn start
```

Application starts on port `8080` by default so open your browser and navigate to `http://localhost:8080`

> You can configure default port modified [webpack.config.js](webpack.config.js) `port` property.

## Developing

### Built With

The libraries uses in this project are:

| Project | Package | Version |
|---|---|---|
**@babel/cli** | [`@babel/cli`](https://www.npmjs.com/package/@babel/cli) | ![npm](https://img.shields.io/npm/v/@babel/cli) |  
**@babel/core** | [`@babel/core`](https://www.npmjs.com/package/@babel/core) | ![npm](https://img.shields.io/npm/v/@babel/core) |  
**babel/polyfill** | [`@babel/polyfill`](https://www.npmjs.com/package/@babel/polyfill) | ![npm](https://img.shields.io/npm/v/@babel/polyfill) |
**@babel/preset-env** | [`@babel/preset-env`](https://www.npmjs.com/package/@babel/preset-env) | ![npm](https://img.shields.io/npm/v/@babel/preset-env) |  
**TypeScript loader for Webpack** | [`awesome-typescript-loader`](https://www.npmjs.com/package/awesome-typescript-loader) | ![npm](https://img.shields.io/npm/v/awesome-typescript-loader) |
**Babel Loader**   | [`babel-loader`](https://www.npmjs.com/package/babel-loader) | ![npm](https://img.shields.io/npm/v/babel-loader) |
**css-loader** | [`css-loader`](https://www.npmjs.com/package/css-loader) | ![npm](https://img.shields.io/npm/v/css-loader) |
**ESLint** | [`eslint`](https://www.npmjs.com/package/eslint) | ![npm](https://img.shields.io/npm/v/eslint) |
**webpack** | [`webpack`](https://www.npmjs.com/package/webpack) | ![npm](https://img.shields.io/npm/v/webpack) |
**webpack CLI** | [`webpack-cli`](https://www.npmjs.com/package/webpack-cli) | ![npm](https://img.shields.io/npm/v/webpack-cli) |
**webpack-dev-server** | [`webpack-dev-server`](https://www.npmjs.com/package/webpack-dev-server) | ![npm](https://img.shields.io/npm/v/webpack-dev-server) |
**Style Loader** | [`style-loader`](https://www.npmjs.com/package/style-loader) | ![npm](https://img.shields.io/npm/v/style-loader) |
**file-loader** | [`file-loader`](https://www.npmjs.com/package/file-loader) | ![npm](https://img.shields.io/npm/v/file-loader) |
**HTML Webpack Plugin** | [`html-webpack-plugin`](https://www.npmjs.com/package/html-webpack-plugin) | ![npm](https://img.shields.io/npm/v/html-webpack-plugin) |
**mini-css-extract-plugin** | [`mini-css-extract-plugin`](https://www.npmjs.com/package/mini-css-extract-plugin) | ![npm](https://img.shields.io/npm/v/mini-css-extract-plugin) |
**TypeScript** | [`typescript`](https://www.npmjs.com/package/typescript) | ![npm](https://img.shields.io/npm/v/typescript) |
**core-js** | [`core-js`](https://www.npmjs.com/package/core-js) | ![npm](https://img.shields.io/npm/v/core-js) |
**React** | [`react`](https://www.npmjs.com/package/react) | ![npm](https://img.shields.io/npm/v/react) |
**React-DOM** | [`react-dom`](https://www.npmjs.com/package/react-dom) | ![npm](https://img.shields.io/npm/v/react-dom)|
**@types/react** | [`@types/react`](https://www.npmjs.com/package/@types/react) | ![npm](https://img.shields.io/npm/v/@types/react) |
**@types/react-dom** | [`@types/react-dom`](https://www.npmjs.com/package/@types/react-dom) | ![npm](https://img.shields.io/npm/v/@types/react-dom) |
**React Weather Icons** | [`react-weathericons`](https://www.npmjs.com/package/react-weathericons) | ![npm](https://img.shields.io/npm/v/react-weathericons)
**prop-types** | [`prop-types`](https://www.npmjs.com/package/prop-types) | ![npm](https://img.shields.io/npm/v/prop-types)

### Prerequisites

| Project | Package | Version | Links |
|---|---|---|---|
**rimraf** | [`rimraf`](https://www.npmjs.com/package/rimraf) | ![npm](https://img.shields.io/npm/v/rimraf) | [![README](https://img.shields.io/badge/README--green.svg)](https://github.com/isaacs/rimraf/blob/master/README.md) |
**husky** | [`husky`](https://www.npmjs.com/package/husky) | ![npm](https://img.shields.io/npm/v/husky) | [![README](https://img.shields.io/badge/README--green.svg)](https://github.com/typicode/husky/blob/master/README.md) |
**npm-check-updates** | [`npm-check-updates`](https://www.npmjs.com/package/npm-check-updates) | ![npm](https://img.shields.io/npm/v/npm-check-updates) | [![README](https://img.shields.io/badge/README--green.svg)](https://github.com/tjunnone/npm-check-updates/blob/master/README.md) |

### Setting up Dev

Clone this repository with this command:

```bash
git clone https://gitlab.com/notions/react/weather-app.git
```

Then you have to install project's dependencies:

```bash
cd weather-app
yarn
```

Once project's dependencies have been installed, you can run this command to start the application:

```bash
yarn start
```

Application starts on port `8080` by default so open your browser and navigate to `http://localhost:8080`.

### Linting

ESLint is installed and condifured with recommended rules. You can revise linter configuration in [.eslintrc.json](.eslintrc.json) file.

A script is provided in [package.json](package.json) to run eslint:

```bash
yarn lint
```

> Husky is configured to run eslint on _pre-commit_ with `'--fix'` flag and witouth it on _pre-push_ git hooks.

### Building

To compile the project, you have to execute this command:

```bash
yarn build
```

> The generate bundle of the application with the command above isn't optimized for the production environment and it's thought to do testing on a pre-production server.

The command avobe generates the `dist` folder with the bundle of the application.

### Deploying

The configuration required to deploy the application isn't provided.

## Versioning

It's recommended to use [SemVer](https://semver.org/). You can find more information about Semantic Versioning on their website.

## Tests

The configuration required to test the application isn't provided.

## Licensing

Not provided yet.
